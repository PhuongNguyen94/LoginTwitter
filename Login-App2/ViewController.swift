//
//  ViewController.swift
//  Login-App2
//
//  Created by Administrator on 1/19/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import TwitterKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
   
        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
            if (session != nil) {
                
                print("signed in as \(String(describing: session?.userName))");
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                let composer = TWTRComposer()
                
                
                print("signed in as \(String(describing: session?.userName))");
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)
   
        
        
    }
    
  
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

